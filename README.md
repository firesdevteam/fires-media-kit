# FIRES Media Kit

This is a simple little kit containing new logos for FIRES that are available in high resolution and vector formats.  Please only use the download zips provided, as other logos and images may be outdated.